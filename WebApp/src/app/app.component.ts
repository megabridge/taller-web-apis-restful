import { Component, OnInit } from '@angular/core';
import { TodoDataService } from './services/todo-data.service';
import { TodoEntity } from './models/todo';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [TodoDataService]
})
export class AppComponent implements OnInit {

  newTodo: TodoEntity = new TodoEntity();

  todos: TodoEntity[] = [];

  constructor(private todoDataService: TodoDataService) {
  }

  public ngOnInit() {
    this.todoDataService
      .getAllTodos()
      .subscribe(
        (res) => {
          this.todos = res.Data;
        }
      );
  }

  addTodo(todo) {
    this.todoDataService
    .createTodo(todo)
    .subscribe(
      (res) => {
        this.todos = this.todos.concat(res.Data);
        this.newTodo = new TodoEntity();
      }
    );
  }

  toggleTodoComplete(todo) {
    this.todoDataService
      .toggleTodoComplete(todo)
      .subscribe(
        (res) => {
          todo = res.Data;
        }
      );
  }

  removeTodo(todo) {
    this.todoDataService
    .deleteTodoById(todo.id)
    .subscribe(
      (_) => {
        this.todos = this.todos.filter((t) => t.id !== todo.id);
      }
    );
  }

}
