import { Injectable } from '@angular/core';
import { TodoEntity } from '../models/todo';
import { environment } from 'src/environments/environment';

import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class TodoDataService {
  constructor(
    private http: Http
  ) {
  }

  public RequestOptions(params?: any) {
    params = params || {};

    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    return new RequestOptions({ headers: headers, params: params });
  }

  // API: GET /todos
  public getAllTodos(): Observable<any> {
    return this.http
      .get(API_URL + '/Todo/GetAll').pipe(map(response => {
        return response.json();
      }));
  }

  // API: POST /todos
  public createTodo(todo: TodoEntity): Observable<any> {
    return this.http
      .post(API_URL + '/Todo/Post', todo, this.RequestOptions()).pipe(map(response => {
        return response.json();
      }));
  }

  // API: GET /todos/:id
  public getTodoById(todoId: number): Observable<any> {
    return this.http
      .get(API_URL + '/Todo/GetById' + todoId, this.RequestOptions()).pipe(map(response => {
        return response.json();
      }));
  }

  // API: PUT /todos/:id
  public updateTodo(todo: TodoEntity): Observable<any> {
    todo.complete = !todo.complete;
    return this.http
      .put(API_URL + '/Todo/Put', todo, this.RequestOptions()).pipe(map(response => {
        return response.json();
      }));
  }

  // DELETE /todos/:id
  public deleteTodoById(todoId: number): Observable<any> {
    return this.http
      .delete(API_URL + '/Todo/Delete?idItem=' + todoId.toString(), this.RequestOptions()).pipe(map(response => {
        return response.json();
      }));
  }

  // Toggle todo complete
  toggleTodoComplete(todo: TodoEntity) {
    const updatedTodo = this.updateTodo(todo);
    return updatedTodo;
  }

}
