export class TodoEntity {
  id = 0;
  name = '';
  complete = false;

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}
