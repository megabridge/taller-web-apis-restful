CREATE TABLE [dbo].[todoitems](
	[Id_Todo_Item] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Is_Complete] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id_Todo_Item] ASC
)
)

