﻿using DesktopApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DesktopApp
{
    public partial class Todos : Form
    {
        public Todos()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private async void GetAll()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:64123/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));

                    using (var response = await client.GetAsync("api/Todo/GetAll"))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            string productJsonString = await response.Content.ReadAsStringAsync();
                            CustomResponse casted = JsonConvert.DeserializeObject<CustomResponse>(productJsonString);

                            dgvTodos.DataSource = JsonConvert.DeserializeObject<TodoEntity[]>(casted.Data.ToString()).ToList();

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private async void GetById(int id)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:64123/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));

                    using (var response = await client.GetAsync($"api/Todo/GetById?id={id}"))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var productJsonString = await response.Content.ReadAsStringAsync();
                            var casted = JsonConvert.DeserializeObject<CustomResponse>(productJsonString);

                            TodoEntity todo = JsonConvert.DeserializeObject<TodoEntity>(casted.Data.ToString());

                            txtId.Text = todo.id.ToString();
                            txtTodo.Text = todo.name;
                            chkComplete.Checked = todo.complete;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private async void AddTodo()
        {
            try
            {
                TodoEntity newTodo = new TodoEntity();
                newTodo.id = 0;
                newTodo.name = txtTodo.Text;
                newTodo.complete = chkComplete.Checked;

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:64123/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));

                    var serializedProduct = JsonConvert.SerializeObject(newTodo);
                    var content = new StringContent(serializedProduct, Encoding.UTF8, "application/json");
                    var result = await client.PostAsync("api/Todo/Post", content);
                }
                GetAll();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private async void UpdateTodo()
        {
            try
            {
                TodoEntity todo = new TodoEntity();
                todo.id = Convert.ToInt32(txtId.Text);
                todo.name = txtTodo.Text;
                todo.complete = chkComplete.Checked;

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:64123/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));

                    var serializedProduct = JsonConvert.SerializeObject(todo);
                    var content = new StringContent(serializedProduct, Encoding.UTF8, "application/json");
                    var result = await client.PutAsync("api/Todo/Put", content);
                }
                GetAll();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }


        private async void DeleteTodo()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:64123/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));

                    var result = await client.DeleteAsync($"api/Todo/Delete?idItem={txtId.Text}");
                }
                GetAll();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btnGetAll_Click(object sender, EventArgs e)
        {
            try
            {
                GetAll();   
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            GetAll();
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            AddTodo();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            UpdateTodo();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteTodo();
        }

        private void dgvTodos_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                var prueba = dgvTodos.Rows[e.RowIndex];
                var idCell = prueba.Cells[0];
                var id = Convert.ToInt32(idCell.Value);
                GetById(id);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
    }
}
