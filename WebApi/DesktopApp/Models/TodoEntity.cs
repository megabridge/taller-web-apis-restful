﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesktopApp.Models
{
    public class TodoEntity
    {
        public int id { get; set; }
        public string name { get; set; }
        public bool complete { get; set; }
    }
}
