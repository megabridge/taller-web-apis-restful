﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi.Models;

namespace WebApi.Controllers
{
    [RoutePrefix("api/Todo")]
    public class TodoController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        [HttpGet]
        [Route("GetAll")]
        public HttpResponseMessage GetAll()
        {
            
            CustomResponse response = new CustomResponse();
          
            try
            {
                using (TodoEntities db = new TodoEntities())
                {
                    var allTodos = (from usr in db.Users
                                    join tdi in db.todoitems on usr.Id_User equals tdi.Id_User
                                    select new TodoEntity()
                                    {
                                        id = tdi.Id_Todo_Item,
                                        name = tdi.Name,
                                        complete = tdi.Is_Complete ?? false,
                                        userName = usr.Name
                                    }
                                    ).ToList();
                    if (allTodos.Any())
                    {
                        response.Status = true;
                        response.Message = "Tareas cargadas correctamente";
                        response.Data = allTodos;
                        logger.Info("Tareas cargadas correctamente" + Environment.NewLine + DateTime.Now);
                    }
                    else
                    {
                        response.Status = false;
                        response.Message = "Sin tareas registradas";
                        response.Data = null;
                    }
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = ex.Message;
                response.Data = null;
                logger.Error(ex.Message + Environment.NewLine + DateTime.Now);
            }

            return Request.CreateResponse(response);
        }

        [HttpGet]
        [Route("GetById")]
        public HttpResponseMessage GetById(int id)
        {
            CustomResponse response = new CustomResponse();

            try
            {
                using (TodoEntities db = new TodoEntities())
                {
                    TodoEntity todo = db.todoitems.Where(w => w.Id_Todo_Item == id)
                        .Select(s => new TodoEntity()
                        {
                            id = s.Id_Todo_Item,
                            name = s.Name,
                            complete = s.Is_Complete ?? false
                        }).FirstOrDefault();

                    if (todo != null)
                    {
                        response.Status = true;
                        response.Message = "Tarea cargada correctamente";
                        response.Data = todo;
                    }
                    else
                    {
                        response.Status = false;
                        response.Message = "No se econtro la tarea";
                        response.Data = null;
                    }
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = ex.Message;
                response.Data = null;
                logger.Error(ex.Message + Environment.NewLine + DateTime.Now);
            }

            return Request.CreateResponse(response);
        }

        [HttpPost]
        [Route("Post")]
        public HttpResponseMessage Post(TodoEntity newItem)
        {
            CustomResponse response = new CustomResponse();

            try
            {
                using (TodoEntities db = new TodoEntities())
                {
                    int idUserName = db.Users.Where(w => w.Name == newItem.userName).Select(s => s.Id_User).FirstOrDefault();

                    todoitems itemCreado = db.todoitems.Add(new todoitems()
                    {
                        Name = newItem.name,
                        Is_Complete = newItem.complete,
                        Id_User = idUserName
                    });
                    db.SaveChanges();

                    if (itemCreado.Id_Todo_Item > 0)
                    {
                        newItem.id = itemCreado.Id_Todo_Item;
                        response.Status = true;
                        response.Message = "Tarea creada correctamente";
                        response.Data = newItem;
                    }
                    else
                    {
                        response.Status = false;
                        response.Message = "No se pudo registrar la tarea";
                        response.Data = null;
                    }
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = ex.Message;
                response.Data = null;
                logger.Error(ex.Message + Environment.NewLine + DateTime.Now);
            }

            return Request.CreateResponse(response);
        }

        [HttpPut]
        [Route("Put")]
        public HttpResponseMessage Put(TodoEntity item)
        {
            CustomResponse response = new CustomResponse();

            try
            {
                using (TodoEntities db = new TodoEntities())
                {
                    var itemGuardado = db.todoitems.Where(w => w.Id_Todo_Item == item.id).FirstOrDefault();

                    if (itemGuardado != null)
                    {
                        itemGuardado.Is_Complete = item.complete;
                        itemGuardado.Name = item.name;
                        db.SaveChanges();

                        response.Status = true;
                        response.Message = "Tarea actualizada correctamente";
                        response.Data = item;
                    }
                    else
                    {
                        response.Status = false;
                        response.Message = "No se pudo actualizar la tarea";
                        response.Data = null;
                    }
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = ex.Message;
                response.Data = null;
                logger.Error(ex.Message + Environment.NewLine + DateTime.Now);
            }

            return Request.CreateResponse(response);
        }
        [HttpPut]
        [Route("UpdateAllToCheck")]
        public HttpResponseMessage UpdateAllToCheck()
        {
            CustomResponse response = new CustomResponse();

            try
            {
                using (TodoEntities db = new TodoEntities())
                {
                    List<todoitems> todo = db.todoitems.Where(w => !w.Is_Complete.Value).ToList();

                    if (todo != null)
                    {
                        todo.ForEach(f => f.Is_Complete = true);
                        db.SaveChanges();

                        response.Status = true;
                        response.Message = "Tareas finalizadas correctamente";
                        response.Data = todo.Count;
                    }
                    else
                    {
                        response.Status = false;
                        response.Message = "No se econtraron tareas";
                        response.Data = null;
                    }
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = ex.Message;
                response.Data = null;
                logger.Error(ex.Message + Environment.NewLine + DateTime.Now);
            }

            return Request.CreateResponse(response);
        }

        [HttpDelete]
        [Route("Delete")]
        public HttpResponseMessage Delete(int idItem)
        {
            CustomResponse response = new CustomResponse();

            try
            {
                using (TodoEntities db = new TodoEntities())
                {
                    var todo = db.todoitems.Where(w => w.Id_Todo_Item == idItem).FirstOrDefault();

                    if (todo != null)
                    {
                        db.todoitems.Remove(todo);
                        db.SaveChanges();

                        response.Status = true;
                        response.Message = "Tarea eliminada correctamente";
                        response.Data = todo;
                    }
                    else
                    {
                        response.Status = false;
                        response.Message = "No se econtro la tarea";
                        response.Data = null;
                    }
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = ex.Message;
                response.Data = null;
                logger.Error(ex.Message + Environment.NewLine + DateTime.Now);
            }

            return Request.CreateResponse(response);
        }
    }
}
