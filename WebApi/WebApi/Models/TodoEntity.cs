﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class TodoEntity
    {
        public int id { get; set; }
        public string name { get; set; }
        public bool complete { get; set; }
        public string userName { get; set; }
    }
}