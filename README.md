# Taller de Web APIs con .NET

Segun wikipedia...una API es una interfaz de programación de aplicaciones. Es un conjunto de rutinas que provee acceso a funciones de un determinado software. ¿Adivinen donde?, asi es! por la web. Mediante el protocolo HTTP.

## ¿Que es un Web API?

Una API (siglas de ‘Application Programming Interface’) es un conjunto de reglas (código) y especificaciones que las aplicaciones pueden seguir para comunicarse entre ellas: sirviendo de interfaz entre programas diferentes de la misma manera en que la interfaz de usuario facilita la interacción humano-software.

Las API son valiosas, ante todo, porque permiten hacer uso de funciones ya existentes en otro software (o de la infraestructura ya existente en otras plataformas) para no estar reinventando la rueda constantemente, reutilizando así código que se sabe que está probado y que funciona correctamente. En el caso de herramientas propietarias (es decir, que no sean de código abierto), son un modo de hacer saber a los programadores de otras aplicaciones cómo incorporar una funcionalidad concreta sin por ello tener que proporcionar información acerca de cómo se realiza internamente el proceso.

Ejemplos de uso:

* Los desarrolladores de un programa cualquiera para Windows que se conecte a Internet no necesitan incluir en su código las funciones necesarias para reconocer la tarjeta de red, por ejemplo: basta una ‘llamada’ a la API correspondiente del sistema operativo.

* Las plataformas relacionadas con Twitter, sean las de búsqueda y filtrado de menciones como Topsy, o las de gestión de tuits como Hootsuite, pueden hacer uso de la capacidad de procesamiento de los servidores de Twitter.

* Los webmasters pueden incluir en sus webs de forma automática productos actualizados de Amazon o eBay, permitiendo iniciar el proceso de compra desde su web. O quizá te permiten identificarte con Facebook Connect, ahorrándose así el complejo proceso de gestión de usuarios (y evitando al internauta tener que registrarse en una web más). Igualmente, los botones de “+1” de los blogs son llamadas a la API de Google.

En la web, las API's son publicadas por sitios para brindar la posibilidad de realizar alguna acción o acceder a alguna característica o contenido que el sitio provee. Algunas de las más conocidas son las API's de:

* Google Search <https://code.google.com/>
* Flickr <https://www.flickr.com/services/api/>
* Google Maps <https://web.archive.org/web/20051126033638/http://www.google.com/apis/maps/>

## ¿Que es HTTP y HTTPS?

HTTP son las siglas en inglés de HiperText Transfer Protocol (en español, protocolo de transferencia de hipertexto). Es un protocolo de red (un protocolo se puede definir como un conjunto de reglas a seguir) que se utiliza para publicar páginas de web o HTML. HTTP es la base sobre la cual se fundamenta Internet o la WWW.

La primera versión documentada de HTTP es la versión 0.9, publicada en 1991.

Puedes ver la especificación de la versión 0.9 de HTTP en esta página de WC3.

HTTP es el protocolo que permite enviar documentos de un lado a otro en la web. Un protocolo es un conjunto de reglas que determina qué mensajes se pueden intercambiar y qué mensajes son respuestas apropiadas a otros.

En HTTP, hay dos funciones diferentes: servidor y cliente. En general, el cliente siempre inicia la conversación; El servidor responde.

HTTP está basado en texto; Es decir, los mensajes son esencialmente bits de texto, aunque el cuerpo del mensaje también puede contener otros medios. El uso del texto facilita el monitoreo de un intercambio HTTP.

Los mensajes HTTP se hacen de un encabezado y un cuerpo. El cuerpo a menudo puede permanecer vacío; Contiene los datos que desea transmitir a través de la red, con el fin de utilizarlo de acuerdo con las instrucciones en el encabezado. El encabezado contiene metadatos, como la información de codificación; Pero, en el caso de una solicitud, también contiene los métodos HTTP importantes. En el estilo REST, se encontrará que los datos de cabecera suelen ser más significativos que el cuerpo.

### Cómo funciona el protocolo HTTP

El protocolo HTTP funciona a través de solicitudes y respuestas entre un cliente (por ejemplo un navegador de Internet) y un servidor (por ejemplo la computadora donde residen páginas web). A una secuencia de estas solicitudes se le conoce como sesión de HTTP.

Un cliente envía una solicitud al servidor (conocida como http request). El servidor le responde al cliente (http response) con información, que puede ser código HTML, una imagen o alguna otra cosa que el cliente sabe interpretar. Cuando visitas una página usando tu navegador de Internet, estás usando una sesión de http para obtener todo lo que ves como resultado final: código HTML, imágenes, código JavaScript, etc.

La URI (más conocida como URL) que muestra el navegador de internet en la llamada “barra de navegación” comienza con http, lo que está indicando que se está usando el protocolo http para que se te muestre la página que estás visitando.

### Sesiones seguras HTTPS

Cuando un URI comienza con HTTPS en lugar de HTTP, significa que el navegador está usando un esquema seguro para proteger la información que está siendo transferida. Este esquema HTTPS es el que debe de usar toda transacción comercial en Internet. A este esquema se le conoce como TSL.

Una sesión segura se diferencia de una no segura en que la sesión segura agrega criptografía para codificar la información transmitida, de tal suerte que si alguien intercepta la comunicación el contenido de los mensajes no pueda ser descifrado sin contar con las llaves correctas.

Otro aspecto de una sesión segura es que durante ésta se emplean certificados para garantizar que la comunicación se está estableciendo con quien se debe, evitando así un intermediario impostor que intercepte la comunicación.

Son cada vez más las páginas en Internet que emplean conexiones seguras (HTTPS). Hacia el final de 2015 ya casi una tercera parte de las páginas web más populares cuentan con una versión segura.

### Ejemplo de una comunicación HTTP

A continuación puedes ver un ejemplo simple de una solicitud de una página, hecha desde un navegador:

El cliente hace una solicitud (en este caso un navegador de Internet) para ver la página principal.

``` HTTP
GET /principal.html HTTP/1.1

Host: www.pagina.com
```

El servidor envía una respuesta al cliente.

``` HTTP
HTTP/1.1 200 OK

Date: Tue, 24 Feb 2016 22:38:34 GMT

Content-Type: text/html; charset=UTF-8

Content-Length: 75

Connection: close

<html>

<head>

  <title>Título</title>

</head>

<body>

  Nada aquí.

</body>

</html>
```

La respuesta está simplificada, pero pudiera llevar mucha más información referente a la versión de la página.

### URLs

Las URL son para identificar las cosas en las que desea operar. Decimos que cada URL identifica un recurso. Estas son exactamente las mismas URL que se asignan a las páginas web. De hecho, una página web es un tipo de recurso. Tomemos un ejemplo más exótico y consideremos nuestra aplicación de ejemplo, que gestiona la lista de clientes de una empresa:

``` HTTP
/clients
```

Identificará a todos los clientes, mientras

``` HTTP
/clients/jim
```

Identificará al cliente, llamado 'Jim', asumiendo que él es el único con ese nombre.

En estos ejemplos, generalmente no incluimos el nombre del host en la URL, ya que es irrelevante desde el punto de vista de cómo se organiza la interfaz. Sin embargo, el nombre de host es importante para asegurar que el identificador de recurso sea único en toda la web. A menudo decimos que envía la solicitud de un recurso a un host. El host se incluye en el encabezado por separado de la ruta de recursos, que viene justo encima del encabezado de la solicitud:

```HTTP
GET /clients/jim HTTP/1.1

Host: example.com
```

Los recursos se consideran mejor como sustantivos. Por ejemplo, lo siguiente no es RESTful:

``` HTTP
/clients/add
```

Esto se debe a que utiliza una URL para describir una acción. Este es un punto bastante fundamental para distinguir los sistemas RESTful de no-RESTful.

Por último, las URL deben ser tan precisas como sea necesario; Todo lo necesario para identificar de forma exclusiva un recurso debe estar en la URL. No es necesario incluir datos que identifiquen el recurso en la solicitud. De esta manera, las URL actúan como un mapa completo de todos los datos que maneja su aplicación.

Pero, ¿cómo se especifica una acción? Por ejemplo, ¿cómo dices que quieres crear un nuevo registro de cliente en lugar de recuperarlo? Aquí es donde entran en juego los verbos HTTP.

### Verbos HTTP

Cada solicitud especifica un cierto verbo o método HTTP, en el encabezado de la solicitud. Esta es la primera palabra de todos las mayúsculas en el encabezado de la solicitud. Por ejemplo,

``` HTTP
GET / HTTP/1.1
```

Significa que se está utilizando el método GET, mientras

``` HTTP
DELETE /clients/anne HTTP/1.1
```

significa que se utiliza el método DELETE.

Los verbos HTTP le indican al servidor qué hacer con los datos identificados por la URL.

La solicitud puede contener opcionalmente información adicional en su cuerpo, que podría ser necesaria para realizar la operación, por ejemplo, los datos que desea almacenar con el recurso.

Si alguna vez has creado formularios HTML, estarás familiarizado con dos de los verbos HTTP más importantes: GET y POST. Pero hay muchos más verbos HTTP disponibles. Los más importantes para la creación de RESTful API son GET, POST, PUT y DELETE. Otros métodos están disponibles, como HEAD y OPTIONS, pero son más raros.

### GET

GET es el tipo más simple de método de solicitud HTTP; La que usan los navegadores cada vez que hace clic en un enlace o escribe una URL en la barra de direcciones. Indica al servidor que transmita los datos identificados por la URL al cliente. Los datos nunca deben ser modificados en el lado del servidor como resultado de una solicitud GET. En este sentido, una petición GET es de sólo lectura, pero por supuesto, una vez que el cliente recibe los datos, es libre de hacer cualquier operación con ella por su cuenta, por ejemplo, formatearla para su visualización.

### POST

POST se utiliza cuando el procesamiento que desea que suceda en el servidor debe repetirse, si la solicitud POST se repite (es decir, no son idempotent, más de esto a continuación). Además, las solicitudes POST deben causar el procesamiento del cuerpo de la solicitud como un subordinado de la URL que está publicando.

En palabras claras:

``` HTTP
POST /clients/
```

No debe causar que el recurso en /clients/, se modifique, sino un recurso cuya URL comience con /clients/. Por ejemplo, podría agregar un nuevo cliente a la lista, con un ID generado por el servidor.

``` HTTP
/clients/some-unique-id
```

Solicitudes PUT se utilizan fácilmente en lugar de solicitudes POST, y viceversa. Algunos sistemas utilizan sólo uno, algunos utilizan POST para crear operaciones y PUT para operaciones de actualización (ya que con una solicitud PUT siempre proporcionan la URL completa), algunos incluso utilizan POST para actualizaciones y PUT para crear.

A menudo, las solicitudes POST se utilizan para activar las operaciones en el servidor, que no encajan en el paradigma Crear / Actualizar / Eliminar; Pero esto, sin embargo, está más allá del alcance de REST. En nuestro ejemplo, nos quedaremos con PUT todo el camino.

### PUT

Una petición PUT se utiliza cuando se desea crear o actualizar el recurso identificado por la URL. Por ejemplo,

``` HTTP
PUT /clients/robin
```

Podría crear un cliente, llamado Robin en el servidor. Usted notará que REST es completamente agnóstico de servidor; No hay nada en la solicitud que informe al servidor cómo deben crearse los datos, sólo que debería. Esto le permite intercambiar fácilmente la tecnología del servidor si la necesidad surge. Las peticiones PUT contienen los datos que se utilizarán para actualizar o crear el recurso en el cuerpo.

### DELETE

DELETE debe realizar lo contrario de PUT; Debe utilizarse cuando desee eliminar el recurso identificado por la URL de la solicitud.

``` HTTP
DELETE /clients/anne
```

Esto eliminará todos los datos asociados con el recurso, identificados por /clients/anne.

### Códigos de respuesta

Los códigos de respuesta HTTP estandarizan una forma de informar al cliente sobre el resultado de su solicitud.

Usted puede ser que haya notado que la aplicación del ejemplo utiliza PHP header(), pasando algunas secuencias que miran extrañas como argumentos. La función header() imprime los headers HTTP y asegura que estén formateados apropiadamente. Headers debe ser la primera cosa en la respuesta, por lo que no debe salir nada más antes de terminar con los encabezados. A veces, su servidor HTTP puede estar configurado para agregar otros encabezados, además de los que especifique en su código.

Los encabezados contienen todo tipo de información meta; Por ejemplo, la codificación de texto utilizada en el cuerpo del mensaje o el tipo MIME del contenido del cuerpo. En este caso, especificamos explícitamente los códigos de respuesta HTTP. Los códigos de respuesta HTTP estandarizan una forma de informar al cliente sobre el resultado de su solicitud. De forma predeterminada, PHP devuelve un código de respuesta de 200, lo que significa que la respuesta es correcta.

El servidor debe devolver el código de respuesta HTTP más apropiado; De esta manera, el cliente puede intentar reparar sus errores, asumiendo que hay alguno. La mayoría de las personas están familiarizadas con el código de respuesta 404 No encontrado, sin embargo, hay mucho más disponible para adaptarse a una amplia variedad de situaciones.

Tenga en cuenta que el significado de un código de respuesta HTTP no es muy preciso; Esto es una consecuencia de HTTP mismo que es bastante genérico. Debe intentar utilizar el código de respuesta que más se aproxima a la situación actual. Dicho esto, no te preocupes demasiado si no puedes encontrar uno que sea exacto.

Estos son algunos códigos de respuesta HTTP, que a menudo se utilizan con REST:

#### 200 OK

Este código de respuesta indica que la solicitud se ha realizado correctamente.

#### 201 Created

Esto indica que la solicitud tuvo éxito y se creó un recurso. Se utiliza para confirmar el éxito de una solicitud PUT o POST.

#### 400 Bad Request

La solicitud fue malformada. Esto sucede especialmente con las solicitudes POST y PUT, cuando los datos no pasan la validación o están en el formato incorrecto.

#### 404 Not Found

Esta respuesta indica que no se pudo encontrar el recurso necesario. Esto generalmente se devuelve a todas las solicitudes que apuntan a una URL sin recurso correspondiente.

#### 401 Unauthorized

Este error indica que debe realizar la autenticación antes de acceder al recurso.

#### 405 Method Not Allowed

El método HTTP utilizado no es compatible con el de este recurso.

#### 409 Conflict

Esto indica un conflicto. Por ejemplo, está utilizando una solicitud PUT para crear el mismo recurso dos veces.

#### 500 Internal Server Error

Cuando todo lo demás falla; En general, se utiliza una respuesta 500 cuando el procesamiento falla debido a circunstancias imprevistas en el lado del servidor, lo que provoca el error del servidor.

![EF6](assets/roadhttp.png)

## ¿Que es REST?

REST deriva de "REpresentational State Transfer", que traducido vendría a ser “transferencia de representación de estado”, lo que tampoco aclara mucho, pero contiene la clave de lo que significa. Porque la clave de REST es que no tiene estado (es stateless), lo que quiere decir que, entre dos llamadas, el servicio pierde todos sus datos. Esto es, que no se puede llamar a un servicio REST y pasarle unos datos (p. ej. un usuario y una contraseña) y esperar que “nos recuerde” en la siguiente petición. De ahí el nombre: el estado lo mantiene el cliente y por lo tanto es el cliente quien debe pasar el estado en cada llamada. Si quiero que un servicio REST me recuerde, debo pasarle quien soy en cada llamada. Eso puede ser un usuario y una contraseña, un token o cualquier otro tipo de credenciales, pero debo pasarlas en cada llamada. Y lo mismo aplica para el resto de información.

El no tener estado es una desventaja clara: tener que pasar el estado en cada llamada es, como mínimo, tedioso, pero la contrapartida es clara: escalabilidad. Para mantener un estado se requiere algún sitio (generalmente memoria) donde guardar todos los estados de todos los clientes. A más clientes, más memoria, hasta que al final podemos llegar a no poder admitir más clientes, no por falta de CPU, sino de memoria. Es algo parecido a lo que ocurre con la web tradicional (que también es stateless). Sea cual sea la tecnología con la que desarrolles para web, seguro que conoces que puedes crear lo que se llama una “sesión”. Los datos de la sesión se mantienen entre peticiones y son por cada usuario. El clásico ejemplo de sesión puede ser un carrito de la compra, entre las distintas peticiones web, la información del carrito de compra se mantiene (¡ojo! hay otras alternativas para implementar carritos de compra).

Por norma general, la sesión se almacena en la memoria RAM del servidor, por lo que es fácil quedarnos sin memoria si introducimos demasiados datos en sesión (o tenemos muchos usuarios simultáneos). Por supuesto, podemos utilizar varios servidores, pero como bien saben los desarrolladores web, mover la sesión contenida en memoria entre servidores es generalmente imposible o altamente ineficiente, lo que penaliza el balanceo de carga, lo que a su vez redunda en menor escalabilidad y menor tolerancia a fallos. Hay soluciones intermedias, tales como guardar la sesión en base de datos, pero su impacto en el rendimiento suele ser muy grande. De hecho, el consejo principal para desarrollar aplicaciones web altamente￼escalables es: no usar la sesión.

Así pues, tenemos que el ser stateless es la característica principal de REST, aunque por supuesto no la única. Así, cualquier servicio REST (si quiere ser merecedor de ese nombre) debería no tener estado, pero no cualquier servicio sin estado es REST.

## Infraestructura de un proyecto ASP.Net

### Controladores

En una web API, un controlador es un objeto que maneja las peticiones HTTP. Estos controladores contienen metodos que se podran invocar mediante el nombre y los argumentos que se indiquen.

### Serializacion

En ASP.NET Web API, un formateador media-type es un objeto que puede:

Leer objetos CLR desde un cuerpo de mensaje HTTP
Escribir objetos CLR en un cuerpo de mensaje HTTP

Web API proporciona formateadores media-type para JSON y XML. El marco inserta estos formateadores en la flujo por defecto. Los clientes pueden solicitar JSON o XML en el encabezado Aceptar de la solicitud HTTP.

### Ruteo

En ASP.NET Web API, un controlador es una clase que maneja las solicitudes HTTP. Los métodos públicos del controlador se llaman métodos de acción o simplemente acciones. Cuando el marco de Web API recibe una solicitud, enruta la solicitud a una acción.

Para determinar qué acción invocar, el marco utiliza una tabla de enrutamiento. La plantilla de proyecto de Visual Studio para Web API crea una ruta predeterminada:

``` CSharp
routes.MapHttpRoute (
     nombre: "API predeterminada",
     routeTemplate: "api / {controller} / {id}",
     valores predeterminados: new {id = RouteParameter.Optional}
);
```

Esta ruta se define en el archivo WebApiConfig.cs, que se coloca en el directorio App_Start:

## Entity Framework

### Linq

LINQ o Language Integrated Query son un conjunto herramientas de Microsoft para realizar todo tipo de consultas a distintas fuentes de datos: objetos, xmls, bases de datos, etc... Para ello, usa un tipo de funciones propias, que unifica las operaciones más comunes en todos los entornos, con esto, se consigue un mismo lenguaje para todo tipo de tareas con datos.

LINQ nace en el Framework 3.5 y pronto coge una gran aceptación entre el mundo de .net, tanto es asi, que enseguida salen proveedores de terceros, para el uso de este lenguaje con JSON, CSVs, o inclusos APIs como la de Twitter y Wikipedia.

La sintaxis es parecida a la existente en SQL, pero con la ventaja de que tenemos la potencia de .net y visual studio a la hora de codificar. Ahora vamos a ver un sencillo ejemplo, con una consulta linq en c# para objetos (LINQ to objetcs):

``` CSharp
var lista = from c in coleccion
            where c.propiedad1 == true
            select c;
```

Como podemos ver, accedemos a una colección y filtramos todos los elementos que cumplan que la propiedad 1 sea verdadera. Del resultado de esta consulta, podemos sacar un listado de elementos con ToList(), el número de ellos con un count(), extraer los datos a un array, etc...

### Operadores Lambda

Aparte del metodo anterior para acceder a las operaciones de LINQ, podemos hacer uso de una sintaxis más directa a la hora de interactuar con LINQ. Esto puede hacerse mediante expresiones de metodos apoyados por operadores lambda. Con ella, podemos llamar directamente a funciones where, join, select, directamente desde el objeto.

``` CSharp
var lista_resultado = lista.where(c => c.id > 5).OrderBy(c => c);
```

En solo una linea de código, hemos cogido un listado, lo hemos filtrado con una condición, y finalmente hemos ordenado los resultados.

Entity Framework 6 (EF6) es un mapeador relacional de objetos probado (O / RM) para .NET con muchos años de desarrollo de características y estabilización.

Como O / RM, EF6 reduce la desigualdad de impedancia entre los mundos relacionales y orientados a objetos, permitiendo a los desarrolladores escribir aplicaciones que interactúan con datos almacenados en bases de datos relacionales utilizando objetos .NET fuertemente tipados que representan el dominio de la aplicación y eliminando la necesidad para una gran parte del código de "plomería" de acceso a datos que generalmente necesitan escribir.

![EF6](assets/EF.jpg)

## Seguridad

### CORS

CORS  (Cross Origin Resource Sharing) es una tecnología que cada día necesitamos utilizar más a la hora de desarrollar aplicaciones móviles y web. ¿Cómo funciona exactamente? . En una aplicación web clásica nosotros podemos cargar una página y solicitar que esta cargue dinámicamente datos vía AJAX.

Eso sí esos datos que vienen vía AJAX tienen que pertenecer al mismo dominio. En el caso de que esto no sea así la aplicación no podrá cargar los datos por limitaciones de seguridad. Los problemas comienzan cuando nosotros tenemos aplicaciones que necesitan acceder a esos datos pero no se encuentran bajo el mismo dominio. Un ejemplo muy habitual es una aplicación Movil empaquetada con PhoneGAP.

En este caso la petición no nos funcionará ya que hemos cargado una página HTML sin descargarla del servidor por lo tanto la página con datos AJAX que solicitamos no está evidentemente en el mismo dominio.

Para habilitar CORS en una aplicacion .net se necesita añadir el paquete Microsoft.AspNet.WebApi.Cors de NuGet al proyecto.

Posteriormente se necesita agregar la siguiente linea de codigo en el App_Start.

``` CSharp
public static void Register(HttpConfiguration config)
{
    // New code
    config.EnableCors();
}
```

### Autenticacion

Es conocer la identidad del usuario. Por ejemplo, Alice inicia sesión con su nombre de usuario y contraseña, y el servidor utiliza la contraseña para autenticar a Alice. Esto le da acceso al sistema o modulos del sistema de una manera segura.

### Autorizacion

Consiste en decidir si un usuario puede realizar una acción. Por ejemplo, Alice dispone del permiso para obtener un recurso pero no crear un recurso. Osea que puede consultar informacion pero no crear registros o actualizarlos.

### OAuth 2 y Web API

OAuth 2 es una estructura (framework) de autorización que le permite a las aplicaciones obtener acceso limitado a cuentas de usuario en un servicio HTTP, como Facebook, GitHub y DigitalOcean. Delega la autenticación del usuario al servicio que aloja la cuenta del mismo y autoriza a las aplicaciones de terceros el acceso a dicha cuenta de usuario. OAuth 2 proporciona flujos de autorización para aplicaciones web y de escritorio; y dispositivos móviles.

Esta guía informativa está dirigida a desarrolladores de aplicaciones; y proporciona una descripción general de los roles de OAuth 2, tipos de autorización, casos de uso y flujos.

### Roles de OAuth

OAuth define cuatro roles:

* Propietario del recurso
* Cliente
* Servidor de recursos
* Servidor de autorización
* Detallaremos cada rol en las siguientes subdivisiones.

Propietario del recurso: Usuario
El propietario del recurso es el "usuario" que da la autorización a una aplicación, para acceder a su cuenta. El acceso de la aplicación a la cuenta del usuario se limita al "alcance" de la autorización otorgada (e.g. acceso de lectura o escritura).

Servidor de Recursos / Autorización: API
El servidor de recursos aloja las cuentas de usuario protegidas, y el servidor de autorizaciones verifica la identidad del usuario y luego genera tokens de acceso a la aplicación.

Desde el punto de vista del desarrollador de una aplicación, la API del servicio atiende tanto a los roles de recursos como a los de autorización. Nos referiremos a ambos roles combinados, como al rol de servicio o de API.

Cliente: Aplicación
El cliente es la aplicación que desea acceder a la cuenta del usuario. Antes de que pueda hacerlo, debe ser autorizado por el usuario, y dicha autorización debe ser validada por la API.

Este es un diagrama de como es el flujo de interaccion general.

![OAuth2](assets/Abstract-Protocol-Flow-Spanish.png)

## Clientes REST

Un cliente, es una aplicacion que se conecta a un servicio que brinda informacion mediante cierto protocolo, con estructuras de informacion definidas por ambos lados.

### Consumo de GET

![OAuth2](assets/getall.png)

``` HTTP
GET /api/Todo/GetAll HTTP/1.1
Host: localhost:64123
Connection: keep-alive
Accept: application/json, text/plain, */*
Origin: http://localhost:4200
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36
Referer: http://localhost:4200/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9,es-US;q=0.8,es;q=0.7
```

### Consumo de POST

![OAuth2](assets/post.png)

``` HTTP
POST /api/Todo/Post HTTP/1.1
Host: localhost:64123
Connection: keep-alive
Content-Length: 59
Accept: application/json
Origin: http://localhost:4200
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36
Content-Type: application/json
Referer: http://localhost:4200/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9,es-US;q=0.8,es;q=0.7

{
  "id": 0,
  "name": "nueva tarea",
  "complete": false
}

```

### Consumo de PUT

![OAuth2](assets/put.png)

``` HTTP
PUT /api/Todo/Put HTTP/1.1
Host: localhost:64123
Connection: keep-alive
Content-Length: 66
Accept: application/json
Origin: http://localhost:4200
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36
Content-Type: application/json
Referer: http://localhost:4200/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9,es-US;q=0.8,es;q=0.7

{
  "id": 2,
  "name": "mi tarea registrada",
  "complete": true
}
```

### Consumo de DELETE

![OAuth2](assets/delete.png)

``` HTTP
DELETE /api/Todo/Delete?idItem=4 HTTP/1.1
Host: localhost:64123
Connection: keep-alive
Content-Length: 0
Accept: application/json
Origin: http://localhost:4200
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36
Content-Type: application/json
Referer: http://localhost:4200/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9,es-US;q=0.8,es;q=0.7
```

## Documentancion

La documentación de software se define como la información enfocada en la descripción del sistema o producto para quienes se encargan de desarrollarlo, implementarlo y utilizarlo.

### Swagger

Swagger nos referimos a una serie de reglas, especificaciones y herramientas que nos ayudan a documentar nuestras APIs.
De esta manera, podemos realizar documentación que sea realmente útil para las personas que la necesitan. Swagger nos ayuda a crear documentación que todo el mundo entienda.

“Swagger es una serie de reglas, especificaciones y herramientas que nos ayudan a documentar nuestras APIs.”

Aunque existen otras plataformas que ofrecen frameworks diferentes, Swagger es la más extendida.

### Swagger UI

Swagger UI es una de las herramientas atractivas de la plataforma. 

Para que una documentación sea útil necesitaremos que sea navegable y que esté perfectamente organizada para un fácil acceso. Por esta razón, realizar una buena documentación puede ser realmente tedioso y consumir mucho tiempo a los desarrolladores.
